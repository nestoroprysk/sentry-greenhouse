package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	sentry "github.com/getsentry/sentry-go"
)

const (
	sentryDSNEnvVar = "SENTRY_DSN"
)

func toString(e interface{}) string {
	result, err := json.MarshalIndent(e, "", "  ")
	if err != nil {
		return fmt.Sprintf("%v", e)
	}

	return string(result)
}

func Log(logf func(format string, v ...interface{}), event *sentry.Event, hint *sentry.EventHint) {
	// const organization = "memsql"
	const organization = "greenhouse-rz"
	eventID := ""
	if event != nil {
		eventID = string(event.EventID)
	}
	logf("Sending the event %q to https://sentry.io/organizations/%s/issues/?query=%s: %s\nWith hint: %s",
		eventID, organization, eventID, toString(event), toString(hint))
}

func main() {
	if err := sentry.Init(sentry.ClientOptions{
		// Using the DSN to connect to the Sentry driver.
		Dsn: mustGetSentryDSN(),
		// Pringing debug info locally to see details.
		Debug: true,
		BeforeSend: func(event *sentry.Event, hint *sentry.EventHint) *sentry.Event {
			Log(log.Printf, event, hint)
			return event
		},
		AttachStacktrace: true,
	}); err != nil {
		log.Fatalf("sentry.Init: %s", err)
	}

	// This is filtered and should be consumed by the inbound filter.
	sentry.CaptureMessage("filtered")

	// Capture.
	cause := fmt.Errorf("for I must be captured")
	sentry.CaptureException(fmt.Errorf("Capture me!: %w", cause))

	// Flush buffered events.
	defer sentry.Flush(2 * time.Second)

	// The panic should be captured.
	defer sentry.Recover()

	panic("Will you catch me panicing?")
}

func mustGetSentryDSN() string {
	result := os.Getenv(sentryDSNEnvVar)
	if result == "" {
		log.Fatal(`export SENTRY_DSN="YOUR SENTRY DSN" to run sentry greenhouse`)
	}

	return result
}
